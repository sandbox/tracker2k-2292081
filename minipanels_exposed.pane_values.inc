<?php
/**
 * @file
 *  minipanels_exposed.pane_values.inc
 *  Micro framework for correct processing of configuration pane forms values.
 */

/**
 * Merges configuration of panes based on it's type, subtype and action.
 *
 * @param $type string Pane type
 * @param $subtype string Pane subtype
 * @param $action string Action that is performed. Can be 'load' or 'save'.
 * @param $conf_basic array Basic configuration of pane in mini panel.
 * @param $conf_new array Configuration that overrides basic configuration.
 * @return array
 */
function minipanels_exposed_pane_values_merge_configuration($type, $subtype, $action, $conf_basic, $conf_new) {
  $function_base = 'minipanels_exposed_pane_values_merge_configuration_';

  $function = $function_base . 'basic';
  if (function_exists($function_base . $type . '_' . $subtype)) {
    $function = $function_base . $type . '_' . $subtype;
  } 
  elseif (function_exists($function_base . $type)) {
    $function = $function_base . $type;
  }

  $allowed_action = array('load', 'save');
  if (! in_array($action, $allowed_action)) {
    $action = 'load';
  }

  if (! is_array($conf_basic)) { $conf_basic = array(); }
  if (! is_array($conf_new)) { $conf_new = array(); }

  return $function($type, $subtype, $action, $conf_basic, $conf_new);
}

/**
 * Callback for basic merge of pane's configuration. If there is no type or subtype specific merge function, this one
 * will be called.
 */
function minipanels_exposed_pane_values_merge_configuration_basic($type, $subtype, $action, $conf_basic, $conf_new) {
  if ($action == 'save') {
    return _minipanels_exposed_array_diff_assoc_recursive($conf_new, $conf_basic);
  } 
  elseif ($action == 'load') {
    return array_replace_recursive($conf_basic, $conf_new);
  }
}

/**
 * Callback for merging aller_content_panes pane configuration
 */
function minipanels_exposed_pane_values_merge_configuration_aller_content_panes($type, $subtype, $action, $conf_basic, $conf_new) {
  if ($action == 'save') {
    // We have some config items that can store arrays, so we can't just marge these values, we can only replace it.
    // So the solution is to cut these array values from new configuration, merge array in normal way, and replace
    // these values in already merged array.

    $exposed = array();

    if (isset($conf_new['exposed'])) {
      foreach ($conf_new['exposed'] as $key => $item) {
        if (is_array($item)) {
          // If new values of 'exposed' array are the same as basic, we can remove it to optimize new configuration
          // array. For simple and fast array compare we can convert it to JSON string, this will be called only from
          // admin area on configuration form submit, so high performance is not necessary here.
          if (isset($conf_basic['exposed'][$key])) {
            $comp_basic = json_encode($conf_basic['exposed'][$key]);
            $comp_new = json_encode($conf_new['exposed'][$key]);
            if ($comp_basic == $comp_new) {
              unset($conf_new['exposed'][$key]);
              continue;
            }
          }

          $exposed[$key] = $item;
          unset($conf_new['exposed'][$key]);

          // Since we are optimizing configuration arrays (removing empty arrays from configuration), we have to preserve
          // out empty value arrays. For that we add internal element to array which will be removed while optimization,
          // but it will inform optimizer to preserve this array even if it's empty
          $exposed[$key]['minipanels_exposed_array_preserve'] = 'minipanels_exposed_array_preserve';
        }
      }
    }

    $conf_merged = _minipanels_exposed_array_diff_assoc_recursive($conf_new, $conf_basic);

    foreach ($exposed as $key => $item) {
      $conf_merged['exposed'][$key] = $item;
    }

    return $conf_merged;
  } 
  elseif ($action == 'load') {
    // We have some config items that can store arrays, so we can't just marge these values, we can only replace it.
    // So the solution is to cut these array values from new configuration, merge array in normal way, and replace
    // these values in already merged array.

    $exposed = array();

    if (isset($conf_new['exposed'])) {
      foreach ($conf_new['exposed'] as $key => $item) {
        if (is_array($item)) {
          $exposed[$key] = $item;
          unset($conf_new['exposed'][$key]);
        }
      }
    }

    $conf_merged = array_replace_recursive($conf_basic, $conf_new);

    foreach ($exposed as $key => $item) {
      $conf_merged['exposed'][$key] = $item;
    }

    return $conf_merged;
  }
}

function minipanels_exposed_pane_values_merge_configuration_style_settings_region($type, $subtype, $action, $conf_basic, $conf_new) {
  if ($action == 'save') {
    $conf_merged = _minipanels_exposed_array_diff_assoc_recursive($conf_new, $conf_basic);

    // If Stylista is disabled in merged of basic configuration then we don't need to store it's settings.
    if (isset($conf_merged['minipanels_exposed_use_stylista'])) {
      if ($conf_merged['minipanels_exposed_use_stylista'] == 0) {
        $conf_merged = array(
          'minipanels_exposed_use_stylista' => 0,
        );
      }
    } elseif ($conf_basic['minipanels_exposed_use_stylista'] == 0) {
      $conf_merged = array();
    }

    return $conf_merged;
  } elseif ($action == 'load') {
    return array_replace_recursive($conf_basic, $conf_new);
  }
}

function minipanels_exposed_pane_values_merge_configuration_style_settings_pane($type, $subtype, $action, $conf_basic, $conf_new) {
  if ($action == 'save') {
    $conf_merged = _minipanels_exposed_array_diff_assoc_recursive($conf_new, $conf_basic);

    // If Stylista is disabled in merged of basic configuration then we don't need to store it's settings.
    if (isset($conf_merged['minipanels_exposed_use_stylista'])) {
      if ($conf_merged['minipanels_exposed_use_stylista'] == 0) {
        $conf_merged = array(
          'minipanels_exposed_use_stylista' => 0,
        );
      }
    } elseif ($conf_basic['minipanels_exposed_use_stylista'] == 0) {
      $conf_merged = array();
    }

    return $conf_merged;
  } elseif ($action == 'load') {
    return array_replace_recursive($conf_basic, $conf_new);
  }
}
