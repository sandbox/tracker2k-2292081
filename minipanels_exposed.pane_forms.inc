<?php
/**
 * @file
 *  minipanels_exposed.pane_forms.inc
 *  Micro framework for correct pane forms generation.
 */

/**
 * Micro framework for pane editing forms
 */
function minipanels_exposed_pane_forms_generate($type, $subtype, $conf_basic, $conf_new, $arguments = array()) {
  $function_base = 'minipanels_exposed_pane_forms_';

  $function = $function_base . 'noform';
  if (function_exists($function_base . $type . '_' . $subtype)) {
    $function = $function_base . $type . '_' . $subtype;
  } 
  elseif (function_exists($function_base . $type)) {
    $function = $function_base . $type;
  }

  if (! is_array($conf_basic)) { $conf_basic = array(); }
  if (! is_array($conf_new)) { $conf_new = array(); }

  return $function($type, $subtype, $conf_basic, $conf_new, $arguments);
}

/**
 * Callback function for all undefined forms for content type plugins
 */
function minipanels_exposed_pane_forms_noform($type, $subtype) {
  global $user;
  $form = array();
  $form[] = array(
    '#type' => 'markup',
    '#markup' =>
      "<div class='messages warning'>" .
      t('Not supported.') .
      (($user->uid == 1) ? "<br>$type - $subtype" : '') .
      "</div>",
  );

  return $form;
}

/**
 * Callback function for mini panels content type plugin
 */
function minipanels_exposed_pane_forms_panels_mini() {
  $form = array();
  $form[] = array(
    '#type' => 'markup',
    '#markup' =>
      "<div class='messages error'>" .
      t('Usage of minipanels inside minipanels is not allowed.') .
      "<br>" .
      t('Please rebuild this panel.') .
      "</div>",
  );

  return $form;
}

/**
 * Callback function for aller_content_panes content type plugin
 */
function minipanels_exposed_pane_forms_aller_content_panes($type, $subtype, $conf_basic, $conf_new, $arguments) {
  $form = array();

  // Micro framework for correct pane configuration merging
  module_load_include('inc', 'minipanels_exposed', 'minipanels_exposed.pane_values');
  $merged_conf = minipanels_exposed_pane_values_merge_configuration($type, $subtype, 'load', $conf_basic, $conf_new);


  // Load default form
  $form_state = array();
  $form_state['conf'] = $merged_conf;
  $form_state['plugin'] = ctools_get_content_type($type);
  $form_state['subtype'] = ctools_content_get_subtype($form_state['plugin'], $subtype);
  $form = ctools_content_configure_form_defaults($form, $form_state);
  $form['override_title']['#id'] .= '-' . $arguments['uuid'];
  $form['override_title_text']['#id'] .= '-' . $arguments['uuid'];
  $form['override_title_text']['#dependency'][$form['override_title']['#id']] = $form['override_title_text']['#dependency']['override-title-checkbox'];
  unset($form['override_title_text']['#dependency']['override-title-checkbox']);
  unset($form['#submit']);


  // Load aller_views pane configuration form
  $form_state = array();
  $form_state['conf'] = $merged_conf;
  $form_state['subtype_name'] = $subtype;
  $form_id = 'aller_views_aller_content_panes_content_type_edit_form';
  module_load_include('inc', 'aller_views', 'plugins/content_types/aller_content_panes');
  $form = $form_id($form, $form_state);
  $form['use_pager']['#id'] .= '-' . $arguments['uuid'];
  $form['pager_id']['#id'] .= '-' . $arguments['uuid'];
  $form['pager_id']['#dependency'][$form['use_pager']['#id']] = $form['pager_id']['#dependency']['use-pager-checkbox'];
  unset($form['pager_id']['#dependency']['use-pager-checkbox']);

  // Invoke hook_form_alter(), hook_form_BASE_FORM_ID_alter(), and
  // hook_form_FORM_ID_alter() implementations.
  $hooks = array(
    'form',
    'form_' . $form_id,
  );
  drupal_alter($hooks, $form, $form_state, $form_id);

  // Fix #parents
  $form_path = $arguments['form_path'];
  $form_path[] = 'exposed';
  foreach ($form['exposed'] as $filter_key => &$filter) {
    $filter_key = (string) $filter_key;
    if (($filter_key[0] != '#') && (is_array($filter))) {
      foreach ($filter as $subfilter_key => &$subfilter) {
        $form_path[] = $subfilter_key;
        $subfilter['#parents'] = $form_path;
        array_pop($form_path);
      }
    }
  }

  return $form;
}

/**
 * Callback function for Stylista region settings
 */
function minipanels_exposed_pane_forms_style_settings_region($type, $subtype, $conf_basic, $conf_new, $arguments) {
  // Micro framework for correct pane configuration merging
  module_load_include('inc', 'minipanels_exposed', 'minipanels_exposed.pane_values');
  $merged_conf = minipanels_exposed_pane_values_merge_configuration($type, $subtype, 'load', $conf_basic, $conf_new);

  $form_id = 'stylista_region_settings';

  module_load_include('inc', 'aller_panelstyles', 'plugins/styles/stylista/stylista');
  $form = $form_id($merged_conf);

  $form['minipanels_exposed_use_stylista'] = array(
    '#type' => 'checkbox',
    '#title' => '<strong>' . t('Use Stylista.') . '</strong>',
    '#default_value' => $merged_conf['minipanels_exposed_use_stylista'],
    '#weight' => -1000,
  );

  return $form;
}

/**
 * Callback function for Stylista region settings
 */
function minipanels_exposed_pane_forms_style_settings_pane($type, $subtype, $conf_basic, $conf_new, $arguments) {
  // Micro framework for correct pane configuration merging
  module_load_include('inc', 'minipanels_exposed', 'minipanels_exposed.pane_values');
  $merged_conf = minipanels_exposed_pane_values_merge_configuration($type, $subtype, 'load', $conf_basic, $conf_new);

  $form_id = 'stylista_settings_pane';

  module_load_include('inc', 'aller_panelstyles', 'plugins/styles/stylista/stylista');
  $form = $form_id($merged_conf);

  $form['minipanels_exposed_use_stylista'] = array(
    '#type' => 'checkbox',
    '#title' => '<strong>' . t('Use Stylista.') . '</strong>',
    '#default_value' => $merged_conf['minipanels_exposed_use_stylista'],
    '#weight' => -1000,
  );

  return $form;
}
